const express = require('express');
const app = express();

const http = require('http').Server(app);
const io = require('socket.io')(http);
const turf = require('turf');
const simulation = require('./drones');

// serve static client build for direct access via localhost
app.use('/static', express.static(__dirname + '/app/build/static'));
app.use(express.static(__dirname + '/app/build'));

const interval = 500;
const traveled = {};
const drones = {};

// only report location if the drone hasn't reached the end of it's linestring
function reportLocation(name, drone, client) {
  return () => {
    const {coordinates, distance, length} = drone;
    if (distance <= length) {
      client.emit('coordinates', {name, coordinates});
    }
  }
}

// interperate the interval in which locations are reported as latency (RTT)
const simulations = simulation.features.map(drone => setInterval(() => {
  // determine where the drone should be
  const currentDistance = interval * (drone.properties.kmh / (60 * 60 * 1000));
  const {properties: {name}} = drone;
  
  if (traveled[name] === undefined) traveled[name] = 0;
  const line = turf.lineString(drone.geometry.coordinates);

  // move drone along linestring projected by where the drone is
  const length = turf.lineDistance(line, 'kilometers');
  const totalDistance = turf.along(line, traveled[name], 'kilometers');

  // increment drone's total distance from beginning of our linestring
  traveled[name] += currentDistance;

  // update the drone's current distance and location
  const {geometry: {coordinates}} = totalDistance;
  if (!drones[name]) drones[name] = {length};
  drones[name].distance = traveled[name];
  drones[name].coordinates = coordinates;
}, interval));

// configure socket server as flight simulator
io.on('connection', (client) => {
  client.on('subscribeToLocation', () => {
    console.log('client is subscribing to location');
    for (const name in drones) {
      setInterval(reportLocation(name, drones[name], client), interval);
    }
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
  console.log('running '+simulations.length+' simulations');
});