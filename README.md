
# Drone Locator

A flight simulation Docker Container written in Node.js and packaged as decoupled Express server and React client 

## Running Locally

This was tested with the Debian snap package `v17.06.2-ce`. Please ensure you have a current version of Docker installed before proceeding and adjust as accordingly if you are [running Docker commands as a non-root user](https://docs.docker.com/install/linux/linux-postinstall/).  

```sh
$ git clone git@bitbucket.org:dcd018/drone-locator.git
$ docker build --tag drone-locator ./drone-locator/
$ docker run -p 8080:3000 drone-locator
```
After confirming the container is running, open a browser and navigate to [http://localhost:8080](http://localhost:8080)

The Express server will serve a static build of the React client from port `3000` by default, however. You can optionally set `HOST` and `PORT` environment variables if serving the client as a microservice or elsewhere.

## Navigating the Dashboard

Name, latitude, longitude, speed and latency for each drone are listed in a table with corresponding map marker locations updated in real-time. Click on a marker to view info about the drone it represents. `Latency Threshold` defaults to 600 seconds (10 minutes) and can be updated at any time. Once a drone stops reporting it's location, latency will increase until it reaches the defined threshold, highlighting it in red. To test functionality, run the server with the default simulation, set `Latency Threshold` to 10 seconds, in 1:10 seconds the second drone should stop reporting it's location and it's latency should increase.

## Abstract
In simulating a real-world scenario where drones continuously report their location via cellular data, I decided on the use of sockets that emit events during an interval, interpreted as latency (RTT), to capture real-time speed and idle thresholds. To determine the distance in Kilometers in which to progress a drone along a given Linestring, the server needed to take into account where a drone should be in the world, projected by a default interval of 500 milliseconds and only report location if the distance between a drone's projected coordinates and the end of the Linestring it travels along increases total distance.

Simulations are configured by a FeatureCollection of Linestrings with two required properties, `name` and `kmh`. The `name` of each drone serves as a unique identifier and `kmh` determines the rate in which each drone's total distance is incremented per iteration. Custom simulations have been tested and are possible by replacing the FeatureCollection exported in `/drones.js` with a GeoJSON FeatureCollection of Linestrings, each having a single set of coordinates.

Rather than determining the client's `Km/h` state by using he `kmh` property taken directly from each Feature, the `Km/h` state is calculated by inspecting packet telemetry available to the client. Unit calculations used by the server to determine where a drone should be before reporting coordinates are invoked in the opposite direction to those used by the client in determining speed and latency. These assumptions could be proven by comparing the value of each Feature's `kmh` property to each drone's current `Km/h` state within the dashboard.