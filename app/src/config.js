const host = process.env.HOST || process.env.REACT_APP_HOST;
const port = process.env.PORT || process.env.REACT_APP_PORT;

const env = {
  host: host || null,
  port: port || null
};

const map = {
  center: {lat: 51.509865, lng: -0.118092},
  zoom: 12
};

export {map, env}