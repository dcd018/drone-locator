import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    textAlign: 'center'
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
});

function List(props) {
  const {
    classes,
    listData,
    latencyThreshold, 
    handleChange
  } = props;

  return (
    <div>
      <TextField
        id="latency-threshold"
        label="Latency Threshold"
        className={classes.textField}
        value={latencyThreshold}
        onChange={handleChange}
        margin="normal"
        type="number"
        style={{ alignSelf: 'flex-end'}}
      /><span >seconds</span>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell>Drones</CustomTableCell>
              <CustomTableCell numeric>Latitude</CustomTableCell>
              <CustomTableCell numeric>Longitude</CustomTableCell>
              <CustomTableCell numeric>Speed</CustomTableCell>
              <CustomTableCell numeric>Latency (RTT)</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData.map(drone => {
              return (
                <TableRow className={classes.row} key={drone.name}>
                  <CustomTableCell component="th" scope="row">
                    {drone.name}
                  </CustomTableCell>
                  <CustomTableCell numeric>{drone.lat}</CustomTableCell>
                  <CustomTableCell numeric>{drone.lng}</CustomTableCell>
                  <CustomTableCell numeric>
                    {(!drone.idle) ? `${drone.speed} Km/h` : '--'}
                  </CustomTableCell>
                  <CustomTableCell numeric>
                    <span style={{color: (!drone.idle) ? 'green' : 'red'}}>
                      {drone.latency} sec
                    </span>
                  </CustomTableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);
