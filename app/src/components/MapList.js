import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {Map, TileLayer, Marker, Popup} from 'react-leaflet';
import List from './List';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 24
  },
  paper: {
    padding: theme.spacing.unit * 2,
    color: theme.palette.text.secondary,
  },
});

function MapList(props) {
  const {
    classes, 
    center, 
    zoom, 
    markers,
    listData,
    latencyThreshold,
    handleChange
  } = props;
  const position = [center.lat, center.lng];

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <List 
              listData={listData}
              latencyThreshold={latencyThreshold}
              handleChange={handleChange}
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <Map center={position} zoom={zoom}>
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
              />
              {markers.map(marker => {
                const {position: {geometry: {coordinates: [lng, lat]}}} = marker 
                return (
                  <Marker key={marker.name} position={[lat, lng]}>
                    <Popup>
                      <strong>{marker.name}</strong><br />
                      Latitude: {lat}<br />
                      Longitude: {lng}
                    </Popup>
                  </Marker>
                )
              })}
            </Map>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

MapList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MapList);