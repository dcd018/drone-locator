import React, {Component} from 'react';
import moment from 'moment';
import turf from 'turf';
import {subscribe} from './api';
import {map} from './config';
import logo from './logo.svg';
import Nav from './components/Nav';
import MapList from './components/MapList';
import './App.css';

const {center, zoom} = map;

class App extends Component {

  state = {center, zoom, markers: [], latencyThreshold: 600};

  constructor(props) {
    super(props);

    this.checkIdle = this.checkIdle.bind(this);
    this.handleChange = this.handleChange.bind(this);

    // subscribe to each drone's location
    subscribe((err, drone) => {
      const {name, coordinates: [lng, lat]} = drone;
      const coordinates = turf.point([lng, lat])
      const idle = false;
      const marker = this.state.markers.find(marker => 
        !!marker && marker.name == name
      );
      const timestamp = moment();
      let markers = [];      
      if (!marker) {
        // if a marker doesn't exist, add it to the current state
        markers = this.state.markers;
        markers.push({name, position: coordinates, timestamp, idle});
      }
      else {
        // if a marker exists, update it's state with a copy of it's 
        // previous state and current location
        markers = this.state.markers;
        markers.forEach(m => {
          if (!!m && marker.name === m.name) {
            m.previousState = JSON.parse(JSON.stringify(m));
            m.position = coordinates;
            m.timestamp = timestamp;
            m.idle = idle;
          }
        })
      }
      this.setState({markers});
    });

    setInterval(this.checkIdle, 1000);
  }

  /**
   * Calculate drone speed
   * 
   * @param  {object} marker Marker representation of drone
   * @return {number}        Drone speed in Km/h 
   */
  calculateSpeed(marker) {
    const {previousState} = marker;

    // determine the difference in time between state changes 
    const latency = (previousState) ? moment.duration(
      marker.timestamp.diff(previousState.timestamp)
    ).asMilliseconds() : 0;

    // determine the distance in kilometers between state changes
    const distance = (previousState) ? turf.distance(
      previousState.position, 
      marker.position, 
      'kilometers'
    ) : 0;

    // as in most cases when distance is < 1km, determine the time it will take to travel at least 1km
    const perKm = (distance < 1) ? latency * (1 / distance) : latency;

    // convert latency to hours
    const perKmHr = ((perKm / 1000) / 60) / 60;

    // determine Km/h
    const kmHr = (1 / perKmHr);
    return Math.round(kmHr);
  }

  /**
   * Calculate latency (RTT) between drone and client
   * 
   * @param  {object} marker Marker representation of drone
   * @return {number}        Latency in seconds
   */
  calculateLatency(marker) {
    const now = moment();
    const latency = moment.duration(now.diff(marker.timestamp));
    return latency.asSeconds();
  }

  /**
   * Check latency threshold of state against drones
   * 
   * @return {void}
   */
  checkIdle() {
    this.state.markers.forEach(marker => {
      const latency = this.calculateLatency(marker);
      marker.idle = latency >= this.state.latencyThreshold;
    });
  }

  /**
   * Export marker data
   * 
   * @return {array} Current state's marker collection
   */
  exportMarkers() {
    return this.state.markers.map(marker => {
      const {name, position: {geometry: {coordinates: [lng, lat]}}, timestamp, idle} = marker;
      const speed = this.calculateSpeed(marker);
      const latency = this.calculateLatency(marker);
      return {name, lat, lng, speed, latency, idle}
    });
  }

  /**
   * Handle latency threshold state change
   * 
   * @param  {object} event Form event
   * @return {void}
   */
  handleChange(event) {
    if (!!event && !!event.target) {
      this.setState({latencyThreshold: event.target.value});
    }
  }

  /**
   * Render MapList component
   * 
   * @return {object} React Component
   */
  render() {
    const position = [
      this.state.center.lat, 
      this.state.center.lng
    ];

    return (
      <div className="App">
        <Nav />
        <MapList 
          center={this.state.center}
          zoom={this.state.zoom}
          markers={this.state.markers}
          listData={this.exportMarkers()}
          latencyThreshold={this.state.latencyThreshold}
          handleChange={this.handleChange}
        />
      </div>
    );
  }
}

export default App;
