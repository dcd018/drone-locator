import io from 'socket.io-client';
import {env} from './config'
const host = (env.host && env.port) ? `http://${env.host}:${env.port}` : null;
const socket = io(host);

/**
 * Listen for location reports
 * @param  {Function} cb Subscription handler
 */
function subscribe(cb) {
  socket.on('coordinates', drone => cb(null, drone));
  socket.emit('subscribeToLocation');
}

export {subscribe};