FROM node:8

# create app directory
WORKDIR /usr/src/app

# bundle app source
COPY . .

# install dependencies
RUN npm install
RUN npm install --prefix ./app

# build React app and start server 
RUN npm run build

# expose listen port
EXPOSE 3000

# start the server
CMD [ "npm", "start" ]
